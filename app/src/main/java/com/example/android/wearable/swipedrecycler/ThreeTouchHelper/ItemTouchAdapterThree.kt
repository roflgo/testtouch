package com.example.android.wearable.swipedrecycler.ThreeTouchHelper

interface ItemTouchAdapterThree {
    fun onMovie(from: Int, to: Int)
    fun onRemoveItem(from: Int)
}