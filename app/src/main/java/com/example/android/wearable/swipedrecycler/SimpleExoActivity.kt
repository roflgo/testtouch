package com.example.android.wearable.swipedrecycler

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.ui.PlayerView

class SimpleExoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_simple_exo)

        val exoPlayer = findViewById<PlayerView>(R.id.players)

        val exoPlayers = ExoPlayer.Builder(this).build()
        exoPlayer.player = exoPlayers
        val mediaStore = MediaItem.fromUri("as")
        exoPlayers.addMediaItem(mediaStore)
        exoPlayers.prepare()
    }
}