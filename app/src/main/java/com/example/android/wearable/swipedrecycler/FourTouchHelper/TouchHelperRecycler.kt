package com.example.android.wearable.swipedrecycler.FourTouchHelper

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.android.wearable.swipedrecycler.R

class TouchHelperRecycler(val context: Context, val listAdapter: MutableList<String>): RecyclerView.Adapter<TouchHelperRecycler.MyVH>(), TouchHelperFourAdapter {
    class MyVH(itemView: View): RecyclerView.ViewHolder(itemView) {
        val holderItem = itemView.findViewById<TextView>(R.id.recyclerSwipe)
    }

    override fun onMovieItem(from: Int, to: Int) {

    }

    override fun onRemoveItem(from: Int) {
     listAdapter.removeAt(0)
     notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyVH {
        return MyVH(LayoutInflater.from(context).inflate(R.layout.recycler_swipe, parent, false))
    }

    override fun onBindViewHolder(holder: MyVH, position: Int) {
        holder.holderItem.text = listAdapter[position]
    }

    override fun getItemCount(): Int {
        return listAdapter.size
    }
}