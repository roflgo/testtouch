package com.example.android.wearable.swipedrecycler.FirstTouchHelper

interface TouchHelperFirstAdapter {
    fun onMoveItem(from: Int, to: Int)
    fun onRemoveItem(from: Int)
}