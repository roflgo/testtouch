package com.example.android.wearable.swipedrecycler

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.TextView
import androidx.constraintlayout.helper.widget.Flow
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.android.wearable.swipedrecycler.FirstTouchHelper.ItemTouchRecycler
import com.example.android.wearable.swipedrecycler.FirstTouchHelper.TouchHelperFirst
import com.example.android.wearable.swipedrecycler.FirstTouchHelper.TouchHelperFirstAdapter
import com.example.android.wearable.swipedrecycler.FourTouchHelper.TouchHelperFour
import com.example.android.wearable.swipedrecycler.FourTouchHelper.TouchHelperFourAdapter
import com.example.android.wearable.swipedrecycler.FourTouchHelper.TouchHelperRecycler
import com.example.android.wearable.swipedrecycler.ThreeTouchHelper.ItemTouchHelperThree
import com.example.android.wearable.swipedrecycler.ThreeTouchHelper.RecyclerViewHelper
import com.example.android.wearable.swipedrecycler.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var flow: Flow
    private lateinit var binding: ActivityMainBinding
    private lateinit var constraint: ConstraintLayout
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val find = findViewById<RecyclerView>(R.id.recyclerView)
        val adapterRec = TouchHelperRecycler(this, mutableListOf("1", "2", "3"))
        find.adapter = adapterRec
        ItemTouchHelper(TouchHelperFour(adapterRec)).attachToRecyclerView(find)

//        ItemTouchHelper(
//            com.example.android.wearable.swipedrecycler.FirstTouchHelper.ItemTouchHelper(
//                adapterRec
//            )
//        ).attachToRecyclerView(find)
        attachFlow()
    }

    fun attachFlow() {
        val list = listOf("1", "2", "3")
        list.map {
            val textWidget = TextView(this)
            textWidget.text = it
            textWidget.id = View.generateViewId()
            textWidget.background = resources.getDrawable(R.drawable.shape_flow)
            textWidget.setTextColor(resources.getColor(R.color.purple_200))
            textWidget.layoutParams  = ViewGroup.LayoutParams(WRAP_CONTENT, WRAP_CONTENT)
            binding.testFlows.addView(textWidget)
            binding.flowDatas.addView(textWidget)
        }

    }
}