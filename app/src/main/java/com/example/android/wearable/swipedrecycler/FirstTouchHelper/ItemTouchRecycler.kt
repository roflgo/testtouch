package com.example.android.wearable.swipedrecycler.FirstTouchHelper

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.android.wearable.swipedrecycler.R

class ItemTouchRecycler(val context: Context, val list: MutableList<String>):
    RecyclerView.Adapter<ItemTouchRecycler.MyVH>(), TouchHelperFirstAdapter {
    class MyVH(itemView: View): RecyclerView.ViewHolder(itemView) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyVH {
        return MyVH(LayoutInflater.from(context).inflate(R.layout.recycler_swipe, parent, false))
    }

    override fun onBindViewHolder(holder: MyVH, position: Int) {

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onMoveItem(from: Int, to: Int) {

    }

    override fun onRemoveItem(from: Int) {
        list.removeAt(0)
        notifyDataSetChanged()
    }
}