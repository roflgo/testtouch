package com.example.android.wearable.swipedrecycler.ThreeTouchHelper

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.android.wearable.swipedrecycler.R

class RecyclerViewHelper(val context: Context, val listText: MutableList<String>):
    RecyclerView.Adapter<RecyclerViewHelper.MyVH>(), ItemTouchAdapterThree {
    class MyVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textMain = itemView.findViewById<TextView>(R.id.recyclerSwipe)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyVH {
        return MyVH(LayoutInflater.from(context).inflate(R.layout.recycler_swipe, parent, false))
    }

    override fun onBindViewHolder(holder: MyVH, position: Int) {
        holder.textMain.text = listText[position]
    }

    override fun getItemCount(): Int {
        return listText.size
    }

    override fun onMovie(from: Int, to: Int) {

    }

    override fun onRemoveItem(from: Int) {
        listText.removeAt(from)
        notifyDataSetChanged()
    }
}