package com.example.android.wearable.swipedrecycler.FourTouchHelper

interface TouchHelperFourAdapter {
    fun onMovieItem(from: Int, to: Int)
    fun onRemoveItem(from: Int)
}